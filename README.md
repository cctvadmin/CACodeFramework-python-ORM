<h1 align="center">Welcome to CACodeFramework 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-Test--1.0.0-blue" />
  <img src="https://img.shields.io/badge/python-%3E%3D%203.6-blue.svg" />
  <a href="https://github.com/Nirongxu/vue-xuAdmin/blob/master/README.md">
    <img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg" target="_blank" />
  </a>
  <a href="https://github.com/Nirongxu/vue-xuAdmin/blob/master/LICENSE">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" target="_blank" />
  </a>
</p>

> pip命令：pip install CACodeFramework

## Author

👤 **CACode**

* Github: [@cctvadmin](https://github.com/cctvadmin)
* QQ: 2075383131
* wechat: cacode
* email: cacode@163.com

## 先决条件

- python >=3.6

项目地址: https://gitee.com/cacode_cctvadmin/CACodeFramework-python-ORM  
项目文档：http://doc.cacode.ren

> 统一市面上所有的数据库创建者，并分配连接池和缓存

## 结语

如果这个框架对你有帮助的话，请给个星点个star

# CACode

!['CACode Development Team'](./imgs/icon_dev.png)
